#pragma once

#include "ofMain.h"
#include "ofxOsc.h"

#include "ofxSyphon.h"
#include "ofxCv.h"
#include "ofxTween.h"
#include "clustering.h"
//#include "simulationEngine.h"

#define MAX_BLOBS 100
#define MAX_TRACKS 32

struct point{
    int ID;
    int	parentID;
    double x;
    double y;
    double z;
    bool active;
    double age;
    int impactPartner;
    bool bound;
    double originDistance;
    double parentDistance;
    double angle;
    bool newTrigger;
    bool prevTrigger;
    bool canTrigger;
    int colorCode;
    ofColor color;
    int deathTime = 0;
    int maxDeathTime = 10;
    
    ofRectangle rect;
    bool direction;
};

class testApp : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void checkForOscMessages();
    void parseTuio(ofxOscMessage m);
    void sendNote(int colorCode, int x, int y, int blobID);
    
    //*******
    void onHand(int bid, float x, float y);
    void shakeScreen();

    void setupOSC();
    void setupMesh();
    void drawShader();
    void sendToMax(string param, float c);
    void sendToMax(string param, float x, float y);
    void sendToMax(string param, int bid, float x, float y);

    void getkmeans();
    
    
    int  tickCounter;
    
    float tickScaling;
    float linePos;
    float lineOffset;
    
    float tickScaling2;
    float linePos2;
    float lineOffset2;
    
    
    float tickScaling3;
    float linePos3;
    float lineOffset3;
    
    
    float tickScaling4;
    float linePos4;
    float lineOffset4;
    
    ofFbo   mFbo;
    
    ofTrueTypeFont TTF;
    ofTrueTypeFont TTFsmall;
    
    
    ofTexture mTex;
    ofxSyphonServer syphonOutTexture;
    
    ofxOscSender    oscSender[3];
    string          sendIP[3];
    int             sendPort[3];
    
    ofxOscReceiver  oscReceiver;
    int             receivePort;
    
    double startTime;
    bool createFlag[100];
    
    point blobs[MAX_BLOBS];
    long maxBlobAlive;
    bool blobStatus[MAX_BLOBS];
    bool prevBlobStatus[MAX_BLOBS];
    bool fseqStatus;
    
    int minimumAge;
    int selectedDirection[MAX_BLOBS];
    int selectionMinimumLength;
    

    int mainWidth = 4*1024;
    int mainHeight = 720;
    
    
    int videoWidth = 720;
    int videoHeight = 480;
    int W = 50; //Grid size
    int H = 50;
    int meshSize = 6;
    ofMesh mesh;
    ofImage image;
    ofImage imageShader;

    ofTexture tex;
    ofPixels fboPixels;
    ofVideoPlayer mPlayer;
    ofShader shader;
    ofTrueTypeFont font;
    ofxOscMessage toMax;
    
    ofEvent<Centroid> handOn;
    
    
    ofxTween shaderAnim;
    ofxTween shaderShake;
    ofxTween shaderInterp;
    
    ofxEasingBack 	easingback;
    ofxEasingBounce 	easingbounce;
    ofxEasingCirc 	easingcirc;
    ofxEasingCubic 	easingcubic;
    ofxEasingElastic easingelastic;
    ofxEasingExpo 	easingexpo;
    ofxEasingLinear 	easinglinear;
    ofxEasingQuad 	easingquad;
    ofxEasingQuart 	easingquart;
    ofxEasingQuint 	easingquint;
    ofxEasingSine	easingsine;
    
    
    bool bAlive = false;
    bool bPrevState = false;
    bool bAnimationLock = false;
    u_int64_t tNow;
    float fAnimInterval = 1000.0f;
    
    std::deque<bool> bListValues;
    
    std::vector<float> blobsAlive;
    int blobCount;
    bool doShader;
    int draggedX, draggedY, clickX, clickY;
    int iRotation;
    
    float iPrevCursorX;
    float iPrevCursorY;

    float iCursorX;
    float iCursorY;
    
    bool mTotalLock = false;
    
    // clustering
    int centroidGroupCount = 4;
    CentroidGroup centroidGroups[4];
    
    void calculateClusters();
    void displayClusters();
    bool mClusterLock = false;


    
};
